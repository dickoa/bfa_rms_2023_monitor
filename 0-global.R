library(tidyverse)
library(sf)
library(robotoolbox)
library(labelled)
library(unhcrthemes)
library(mapboxapi)
library(leaflet)
library(scales)
library(bslib)
library(bsicons)
library(fontawesome)

## Mapbox
mb_access_token(token = Sys.getenv("MAPBOX_UNHCR_TOKEN"),
                install = FALSE,
                overwrite = FALSE)

## Config
kobo_setup(url = Sys.getenv("KOBOTOOLBOX_URL"),
           token = Sys.getenv("KOBOTOOLBOX_DIMA_TOKEN"))

## Get data
raw <- kobo_data(Sys.getenv("KOBOTOOLBOX_RMS_BFA_UID"))

poptype_df <- count(raw$main,
                    poptype = to_character(pop_groups),
                    pop_groups)

poptype_df
geopoint_sf <- raw$main |>
  select(`_id`,
         geopoint_wkt) |>
  drop_na() |>
  st_as_sf(wkt = "geopoint_wkt", crs = 4326) |>
  st_zm(drop = TRUE)
